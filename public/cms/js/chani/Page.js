
var chaniPage = new Class({
	Extends: chaniBlueprintCms,
	options: {
		id: 0,
		ajax: {
			saveValueUrl: '',
			addSpiceUrl: '',
			addPageUrl: '',
			deletePageUrl: '',
			movePageUrl: '',
			previewUrl: '',
			cancelUrl: '',
			resetUrl: ''
		},
		cancelMessage: '',
		className: 'page',
		master: false
	},
	firstStart: true,
	addPageTarget: null,
	addPagePosition: null,
	movePageSubject: null,
	movePageTarget: null,
	movePageClone: null,
	movePageEventRef: null,
	move: false,
	overview: null,
	initialize: function(options) {
		this.setOptions(options);
		this.prepareSitemap();
		this.prepareResetForm();
		this.prepareAddPageModal();
		this.prepareMovePageModal();
		this.parent(options);
	},
	prepareSitemap: function() {
		var self = this;
		this.overview = $('pageOverview');
		this.overview.addEventListener('click',function(event) {
			var target = event.target;
			if(this.move === true) {
				if(target.tagName.toLowerCase() == 'i') {
					//Allow the regular click event to expand sub navigation
					if($(target.parentNode).hasClass('page-sub')) {
						return true;
					}
				}
				var li;
				if(target.tagName.toLowerCase() == 'li') {
					li = target;
				} else {
					li = $(target).getParent('li');
				}
				this.dropPage(li);
				return false;
			}
			if(target.tagName.toLowerCase() == 'i') {
				var parent = $(target.parentNode);
				if(parent.hasClass('page-add')) {
					this.toggleAddPageModal(parent, true, event);
				}
				if(parent.hasClass('page-move')) {
					this.movePage(parent);
				}
				if(parent.hasClass('page-delete')) {
					this.deletePage(parent);
				}
				event.preventDefault();
				event.stopPropagation();
			}
			return false;
		}.bind(self));

		$$('#' + this.overview.id + ' > li > ul > li > ul').each(function(item) {
			var y = item.getSize().y;
			item.setAttribute('data-height',y);
			if(item.getAttribute('data-toggle') == 'open') {
				item.setStyle('height',y);
			} else {
				item.setStyle('height', 0);
				item.setAttribute('data-toggle', 'closed');
			}
		});

		$$('#' + this.overview.id + ' > li > ul').each(function(item) {
			var y = item.getSize().y;
			item.setAttribute('data-height',y);
			if(item.getAttribute('data-toggle') == 'open') {
				item.setStyle('height',y);
			} else {
				item.setStyle('height', 0);
				item.setAttribute('data-toggle', 'closed');
			}
		});

		var els = $$('.page-sub');
		els.each(function(item, index) {
			item.addEvent('click',function(el, event) {
				this.toggleSubPages(el);
				return false;
			}.bind(self, item));
		});

		if(this.options.id == 0 && this.firstStart === true) {
			var sitemapEl = $('sitemap');
			Chani.toggleCMSContent(sitemapEl);
		}
		this.firstStart = false;
	},
	prepareResetForm: function() {
		var reset = $('page-form-reset');
		if(reset) {
			reset.addEvent('submit', function () {
				if (confirm(this.getAttribute('data-confirm'))) {
					return true;
				}
				return false;
			});
		}
	},
	prepareAddPageModal: function() {
		var self = this;
		var els = $$('.page.save, .page.cancel');
		els.each(function(item,index) {
			if(item.hasClass('save')) {
				item.addEvent('click',function(el) {
					this.addPage(el);
				}.bind(self, item));
			}
			if(item.hasClass('cancel')) {
				item.addEvent('click',function(el, event) {
					this.toggleAddPageModal(this.addPageTarget, false, event)
				}.bind(self, item));
			}
		});
	},
	toggleAddPageModal: function(el, open, event) {
		if(open === true) {
			this.addPageTarget = el;
		}
		Chani.toggleModalContent(el, open, true, event)
		if(open === false) {
			this.addPageTarget = null;
		}
	},
	prepareMovePageModal: function() {
		var self = this;
		var els = $$('.page-move.save, .page-move.cancel');
		els.each(function(item,index) {
			if(item.hasClass('save')) {
				item.addEvent('click',function(el) {
					this.doMovePage();
				}.bind(self, item));
			}
			if(item.hasClass('cancel')) {
				item.addEvent('click',function(el, event) {
					console.log('remove page move modal window and clear values in Page object');
					this.toggleMovePageModal(this.movePageTarget, false, event)
				}.bind(self, item));
			}
		});
	},
	toggleMovePageModal: function(el, open, event) {
		if(open === true) {
			$('movePageTitle').innerHTML = $('page-' + this.movePageSubject.getAttribute('data-id')).innerHTML;
			$('moveTargetPageTitle').innerHTML = $('page-' + this.movePageTarget.getAttribute('data-id')).innerHTML;
		} else {
			this.movePageTarget = null;
			this.movePageSubject = null;
		}
		Chani.toggleModalContent(el, open, true, event);
	},
	addPage: function(el) {
		var title = $('addPageTitle').value;
		var positions = $$('#addPagePositionBefore,#addPagePositionAfter,#addPagePositionSub');
		for(var i = 0; i < positions.length; i++) {
			if(positions[i].checked) {
				this.addPagePosition = positions[i].value;
				break;
			}
		}
		var id = this.addPageTarget.getAttribute('data-id');
		var addPage = new Request({
			url: this.options.ajax.addPageUrl,
			data: {
				id: id,
				title: title,
				position: this.addPagePosition
			},
			onSuccess: function(txt) {
				this.replaceOverview(txt);
				this.toggleAddPageModal(this.addPageTarget, false, null);
			}.bind(this)
		});
		addPage.send();
	},
	deletePage: function(el) {
		if(!confirm(el.getAttribute('data-confirm'))) {
			return false;
		}
		var id = el.getAttribute('data-id');
		var deletePage = new Request({
			url: this.options.ajax.deletePageUrl,
			data: {
				id: id
			},
			onSuccess: function(txt) {
				this.replaceOverview(txt);
			}.bind(this)
		});
		deletePage.send();
	},
	replaceOverview: function(txt) {
		var ul = Element('ul');
		ul.id = 'pageOverview';
		ul.innerHTML = txt;
		this.overview.parentNode.replaceChild(ul, this.overview);
		this.overview = null;
		this.prepareSitemap();
	},
	movePage: function(el) {
		this.movePageSubject = $('row_' + el.getAttribute('data-id'));
		var clone = this.movePageSubject.clone();
		clone.addClass('clone');
		this.move = true;
		this.movePageClone =this.overview.appendChild(clone);
		this.movePageEventRef = this.overview.addEvent('mousemove', this.movePageEvent.bind(this));
	},
	movePageEvent: function(event) {
		var pos = this.overview.getPosition();
		var coordinates = {
			top: (event.page.y - pos.y) + 16,
			left: (event.page.x - pos.x) + 16
		};
		if(this.movePageClone) {
			this.movePageClone.setStyles(coordinates);
		}
	},
	dropPage: function(target) {

		this.movePageTarget = target;
		//var targetId = target.getAttribute('data-id');
		//var id = this.movePageTarget.getAttribute('data-id');
		this.move = false;
		this.overview.removeEvent('mousemove',this.movePageEventRef);
		this.overview.removeChild(this.movePageClone);

		this.toggleMovePageModal(this.movePageSubject,true);
		return false;

	},
	doMovePage: function() {
		var position = $('movePagePosition').value;
		var dropPage = new Request({
			url: this.options.ajax.movePageUrl,
			data: {
				targetId: this.movePageTarget.getAttribute('data-id'),
				id: this.movePageSubject.getAttribute('data-id'),
				position: position
			},
			onSuccess: function(txt) {
				this.toggleMovePageModal(this.movePageTarget, false)
				this.replaceOverview(txt);
			}.bind(this)
		});
		dropPage.send();
	},
	handleEvent: function(el, event, params) {
		//Handles the assignment of each event to a field
		//TODO: Change background-color fx to CSS3 instead of Fx.Tween
		var fxSuccess = null;
		for(var i = 0; i < params.length; i++) {
			switch (params[i]) {
				case 'updateTitle':
					el.addEvent(event, function(el) {
						this.updateTitle(el);
					}.bind(this, el));
				case 'toggleTitle':
					el.addEvent(event, function (el) {
						this.toggleTitle(el);
					}.bind(this, el));

					fxSuccess = new Fx.Tween(el, {
						property: 'background-color',
						duration: 1000,
						onComplete: function () {
							if (this.element.getStyle('background-color') == '#111111') {
								this.element.setStyle('background-color', '');
							}
						}
					});
					el.store('fx', fxSuccess);
					this.toggleTitle(el);
					break;
				default:
					this.parent(el, event, [params[i]]);
					break;
			}
		}
	},
	updateTitle: function(el) {
		var value = el.value;
		var title = $('page-title');
		title.innerHTML = value;
	},
	toggleTitle: function(el) {
		var showTitle = $('bShowTitle_' + el.getAttribute('data-id'));
		var title = $('page-title');
		var display = 'none';
		if(showTitle.checked) {
			display = 'block';
		}
		title.style.display = display;
	}
});