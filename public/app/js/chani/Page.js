var BitOfAByte = {

	albums: {},
	activeAlbum: '',
	initialize: function(options) {
		var fullImg = $('media-album-img');
		if(fullImg) {
			fullImg.addEvent('load', function (img) {

			}.bind(this));
		}
		this.printLogo.delay(250, this);
		this.prepareAlbums();
	},
	prepareAlbums: function() {
		var albums = $$('.media-album');
		albums.each(function(album,albumIndex) {
			this.prepareAlbum(album);
		}.bind(this));
	},
	prepareAlbum: function(album) {
		var id = album.id;
		this.albums[id] = {
			index: 0,
			tweens: []
		};
		var pages = album.getElements('.album-page');
		pages.each(function(item, index) {
			var content = item.getElements('.album-content');
			var tmp = [];
			content.each(function(el, i) {
				el.setStyles({
					right: '100%',
					bottom: '100%',
					opacity: 0
				});
				tmp[i] = new TweenMax(el, 0.300, {right: '0', bottom: '0', opacity: 1, paused: true});
			});
			this.albums[id].tweens[index] = tmp;
		}.bind(this));
		this.revealAll.delay(1000, this, [this.albums[id].tweens[this.albums[id].index], false]);
	},
	reveal: function(tween) {
		return tween.play();
	},

	hide: function(tween) {
		return tween.reverse();
	},

	revealAll: function(tweens, reverse) {
		var base = 300;
		var start = 0;
		var end = tweens.length;

		if(reverse) {
			start = end - 1;
			end = 0;

			for(var i = start; i >= end; i--) {
				setTimeout(function (tween) {
					this.hide(tween);
				}.bind(this), base + ((start - i) * 75),tweens[i]);
			}
		} else {
			for (var i = start; i < end; i++) {
				setTimeout(function (tween) {
					this.reveal(tween);
				}.bind(this), base + (i * 75), tweens[i]);
			}
		}
	},

	switchPage: function(id, direction) {
		var album = this.albums[id];
		var el = $(id);
		var activePage = el.getElement('.album-page.active');

		var newPage;
		var index = album.index;
		var newIndex;
		activePage.removeClass('animate');
		if(direction == 'next') {
			activePage.addClass('previous');
			//If next page exists, remove previous class and set next to move it right of the canvas
			if(activePage.nextElementSibling) {
				newPage = activePage.nextElementSibling;
				newIndex = index + 1;
				//Otherwise jump to the first element, reset indexes and add / remove appropriate classes
			} else {
				newPage = el.children[0];
				newIndex = 0;
			}
			newPage.removeClass('next previous animate active');
			newPage.addClass('next');
		} else {
			activePage.addClass('next');
			if(activePage.previousElementSibling) {
				newPage = activePage.previousElementSibling;
				newIndex = index - 1;
			} else {
				var children = el.children;
				newPage = children[children.length - 1];
				newIndex = children.length - 1;
			}
			newPage.removeClass('next previous animate active');
			newPage.addClass('previous');
		}
		activePage.removeClass('active');
		var hides = album.tweens[index];
		var reveals = album.tweens[newIndex];
		this.albums[id].index = newIndex;
		this.revealAll(hides, true);
		this.revealAll(reveals, false);
		newPage.removeClass('next previous');
		newPage.addClass('animate');
		newPage.addClass('active');
	},

	toggleAlbumPageList: function(el) {
		var list = el.parentNode;
		list.toggleClass('reveal');
		return false;
	},

	loadAlbumImg: function(el) {
		var src = el.getAttribute('data-url');
		this.activeAlbum = el.getAttribute('data-album');
		$('media-album-img').src = src;
		var overlay = $('media-album-overlay');
		if(!overlay.hasClass('reveal')) {
			overlay.addClass('reveal');
		}
		return false;
	},
	toggleAlbumOverlay: function() {
		$('media-album-overlay').toggleClass('reveal');
	},

	toggleSubNav: function(id, span) {
		var el = $(id);
		if(el.hasClass('show')) {
			el.removeClass('show');
		} else {
			el.addClass('show');
		}
		return false;
	},

	printPre: function(character) {
		var logo = $('logo');
		logo.innerText = logo.innerText + character;
	},

	printLine: function(line, index) {
		var delay = 0;
		for(var i = 0; i < line.length; i++) {
			delay = (10 * i) + (10 * index);
			setTimeout(function(char) {
				this.printPre(char);
			}.bind(this), 10 + delay, [line[i]]);
		}
	},

	printLines: function(lines) {
		for(var i = 0; i < lines.length; i++) {
			this.printLine(lines[i],i);
		}
	},

	printLogo: function() {
		var logo = $('logo');
		var text = logo.innerText;
		logo.innerText = '';
		text.replace('\r','');
		var line = text.split('');
		this.printLine(line, 0);
	}
};

var tweensReveal = [];
var albumIndex = 0;