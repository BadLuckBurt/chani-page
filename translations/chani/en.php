<?php

	$aMessages = array(
		'module' => 'Pages',
		'add' => 'Add page',
		'sTitle'    =>'Title',
		'sTitleUrl' => 'URL Title',
		'iStatus'   => 'Active',
		'bShowTitle' => 'Show title',
		'bInMenu' => 'Appears in menu',
		'iStatusCode' => 'HTTP status code',
		'iPageId' => 'Load content from page',
		'addSpice' => 'Add spice',
		'saveForm'  => 'Save',
		'processUrl'      => 'Process URL',
		'preview'   => 'Preview',
		'delete'    => 'Delete',
		'confirmDelete' => 'Are you sure you want to delete',
		'move' => 'Move',
		'edit' => 'Edit',
		'cancel' => 'Back to overview',
		'reset' => 'Reset',
		'resetMessage' => 'Do you want to revert to the original page content?',
		'overviewTitle' => 'Sitemap',
		'editTitle' => 'Edit'
	);