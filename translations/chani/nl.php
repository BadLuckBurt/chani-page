<?php

	$aMessages = array(
		'module' => 'Pagina\'s',
		'add' => 'Pagina toevoegen',
		'sTitle'    =>'Titel',
		'sTitleUrl' => 'Titel URL',
		'iStatus'   => 'Actief',
		'bShowTitle' => 'Toon titel',
		'bInMenu' => 'Toon in menu',
		'iStatusCode' => 'HTTP status code',
		'iPageId' => 'Laad content uit pagina',
		'addSpice' => 'Voeg spice toe',
		'saveForm'  => 'Opslaan',
		'processUrl'      => 'Verwerk URL',
		'preview'   => 'Preview',
		'confirmDelete' => 'Klik op OK om de volgende pagina te verwijderen: ',
		'delete' => 'Verwijderen',
		'move' => 'Verplaatsen',
		'edit' => 'Bewerken',
		'cancel' => 'Terug naar overzicht',
		'reset' => 'Herstellen',
		'resetMessage' => 'Wil je de pagina herstellen naar zijn oorspronkelijke inhoud?',
		'overviewTitle' => 'Sitemap',
		'editTitle' => 'Bewerk'
	);