<?php

	namespace Page\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint,
		\Core\Shared,
		\Phalcon\Tag;

	class CmsPageLocale extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iLocaleId;
		public $sTitle;
		public $sTitleUrl;
		public $iStatus;
		public $bEdit;
		public $bShowTitle;
		public $bInMenu;
		public $iPageId;

		public $_model ='CmsPageLocale';

		public function getSource() {
			return 'pagelocale';
		}

		public function initialize() {

			$sClass = str_replace('Locale','',$this->_model);
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			$this->hasOne('iModelId',$sClass,'id', array('alias' => $sClass));

			parent::initialize();
		}

		public function makeEditable() {

			if($this->bEdit == 1) {
				return $this;
			}

			$oEditable = $this->findFirst(array(
				'bEdit = 1 AND iModelId = :iModelId:',
				'bind' => array(
					'iModelId' => $this->iModelId
				)
			));

			if($oEditable === false) {

				$oEditable = static::add($this->iModelId, $this->iLocaleId, 1);
				if($this->sTitle == '') {
					$oEditable->sTitle = new \Phalcon\Db\RawValue('""');
				} else {
					$oEditable->sTitle = $this->sTitle;
				}

				if($this->sTitleUrl == '') {
					$oEditable->sTitleUrl = new \Phalcon\Db\RawValue('""');
				} else {
					$oEditable->sTitleUrl = $this->sTitleUrl;
				}

				$oEditable->iStatus = $this->iStatus;
				$oEditable->bEdit = 1;
				$oEditable->bShowTitle = $this->bShowTitle;
				$oEditable->bInMenu = $this->bInMenu;
				$oEditable->iPageId = $this->iPageId;

				if($oEditable->save() == false) {
					$oMessages = $oEditable->getMessages();
					foreach($oMessages AS $oMessage) {
						echo $oMessage->getMessage();
					}
					die;
				}
			}
			return $oEditable;
		}

		public function save($aData = null, $aWhiteList = null) {
			if($this->sTitle == '' || $this->sTitle === null) {
				$this->sTitle = new \Phalcon\Db\RawValue('""');
			} else {
				$this->sTitleUrl = Tag::friendlyTitle($this->sTitle,'-',true);
			}
			if($this->sTitleUrl == '' || $this->sTitleUrl === null) {
				$this->sTitleUrl = new \Phalcon\Db\RawValue('""');
			}
			return parent::save($aData, $aWhiteList);
		}

		public static function add($iModelId, $iLocaleId, $title = '', $bEdit = 0) {

			$oLocale = new CmsPageLocale();
			$oLocale->dtCreated = Shared::getDBDate();
			$oLocale->dtUpdated = 0;
			$oLocale->iModelId = $iModelId;
			$oLocale->sTitle = $title;
			$oLocale->iLocaleId = $iLocaleId;
			$oLocale->iStatus = 0;
			$oLocale->bEdit = $bEdit;
			$oLocale->bShowTitle = 0;
			$oLocale->bInMenu = 0;
			$oLocale->iPageId = 0;

			if($oLocale->saveData() == false) {
				$oMessages = $oLocale->getMessages();
				foreach($oMessages AS $oMessage) {
					echo $oMessage->getMessage();
				}
				die;
			}
			return $oLocale;
		}
	}