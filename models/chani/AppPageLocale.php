<?php

	namespace Page\Models\Chani;

	use \Core\Models\Chani\AppBlueprint,
		\Core\Shared;

	class AppPageLocale extends AppBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iModelId;
		public $iLocaleId;
		public $sTitle;
		public $sTitleUrl;
		public $iStatus;
		public $bEdit;
		public $bShowTitle;
		public $bInMenu;
		public $iPageId;

		public $_model ='AppPageLocale';

		public function getSource() {
			return 'pagelocale';
		}

		public function initialize() {
			$sClass = str_replace('Locale','',$this->_model);
			$sClass = $this->testClass(__NAMESPACE__,$sClass);
			$this->belongsTo('iModelId',$sClass,'id');
		}

		/**
		 * @param $sTitleUrl
		 * @param $iParentId
		 * @param int $iLocaleId
		 * @param int $bEdit
		 * @return int|\Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model
		 * Resolves a URL title to find the page id
		 */
		public static function resolveUrlTitle($sTitleUrl, $iParentId, $iLocaleId = 116, $bEdit = 0) {

			$id = 0;

			$oLocale = static::findFirst(array(
				'conditions' => 'sTitleUrl = :sTitleUrl: AND iLocaleId = :iLocaleId: AND bEdit = :bEdit:',
				'bind' => array(
					'sTitleUrl' => $sTitleUrl,
					'iLocaleId' => $iLocaleId,
					'bEdit' => $bEdit
				)
			));

			if($oLocale !== false) {

				$aParams = array(
					'conditions' => 'iParentId = :iParentId:',
					'bind' => array('iParentId' => $iParentId)
				);

				$oPage = AppPage::findFirst($aParams);
				if($oPage !== false) {
					$id = $oLocale->iModelId;
				}

			}

			return $id;

		}

	}