<?php

	namespace Page\Models\Chani;

	use \Core\Models\Chani\AppBlueprint,
		\Phalcon\Mvc\Model\Query,
		\L10n\Models\Chani\CmsL10n,
		\Core\Shared,
		\Spice\Models\Chani\AppSpice AS Spice;

	class AppPage extends AppBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iParentId;
		public $iSequence;
		public $iStatusCode;
		public $sIcon;
		public $_model ='AppPage';

		public function getSource() {
			return 'page';
		}

		public function initialize() {

			//The parent::initialize (from Blueprint) Links the Page to Spice
			parent::initialize();

			//Get the class
			$sClass = $this->_model;
			$sLocaleClass = $sClass.'Locale';
			$sLocaleClass = $this->testClass(__NAMESPACE__,$sLocaleClass);

			$this->hasMany('id', $sLocaleClass, 'iModelId',array('alias' => 'locales'));

			$sClass = $this->testClass('Spice\\Models\\Chani','AppSpice');
			$this->hasMany(
				'id',
				$sClass,
				'iModelId',
				array('alias' => 'spice')
			);
		}

		/**
		 * @param null $aConditions
		 * @return \Phalcon\Mvc\Model\ResultsetInterface
		 * Retrieves the content blocks for a page
		 */
		public function getSpice($aConditions = null) {
			if($aConditions === null) {
				$aConditions = array(
					'conditions' => 'bEdit = 0 AND sModel = :sModel:',
					'bind' => array(
						'sModel' => $this->_model
					)
				);
			}
			$oSpice = $this->getRelated('Spice',$aConditions);
			return $oSpice;
		}

		/**
		 * @return array
		 * Returns a list of parent ids for the current page
		 */
		public function getParentIds() {

			$aPageIds = array();
			$aPageIds[] = $this->id;

			if($this->iParentId > 0) {

				$oPage = AppPage::findFirst($this->id);

				while($oPage->iParentId != 0) {

					$oPage = AppPage::findFirst($oPage->iParentId);

					if($oPage !== false) {

						$aPageIds[] = $oPage->id;

					} else {

						break;

					}

				}

			}

			$aPageIds = array_reverse($aPageIds);

			return $aPageIds;
		}

		/**
		 * @param int $iLocaleId
		 * @param int $bEdit
		 * @return string
		 * Creates a URL for the current page
		 */
		public function createUrl($iLocaleId = 116, $bEdit = 0) {

			$sUrl = '';
			$aPageIds = $this->getParentIds();

			for($i = 0; $i < count($aPageIds); $i++) {

				$oLocale = AppPageLocale::findFirst(array(
					'conditions' => 'iModelId = :iModelId: AND iLocaleId = :iLocaleId: AND bEdit = :bEdit:',
					'bind' => array(
						'iModelId' => $aPageIds[$i],
						'iLocaleId' => $iLocaleId,
						'bEdit' => $bEdit
					)
				));

				if($oLocale === false) {

					echo('page could not be found in this language');
					die;

				} else {

					$sUrl .= $oLocale->sTitleUrl.'/';

				}

			}

			return $sUrl;

		}

		/**
		 * @param $iParentId
		 * @param int $iLocaleId
		 * @param int $bEdit
		 * @return \Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model
		 * Returns the id of the first page that can be approached (to redirect contentless pages for example)
		 */
		public static function getFirstActiveId($iParentId, $iLocaleId = 116, $bEdit = 0) {

			$id = $iParentId;

			$oPages = AppPage::find(array(
				'conditions' => 'iParentId = :iParentId:',
				'bind' => array(
					'iParentId' => $iParentId
				),
				'order' => 'iSequence'
			));

			for($i = 0; $i < count($oPages); $i++) {

				$oLocale = AppPageLocale::findFirst(array(
					'iModelId = :id: AND iLocaleId = :iLocaleId: AND bEdit = :bEdit:',
					'bind' => array(
						'id' => $oPages[$i]->id,
						'iLocaleId' => $iLocaleId,
						'bEdit' => $bEdit
					)
				));

				if($oLocale !== false) {

					$id = $oLocale->iModelId;
					break;

				}

			}

			return $id;

		}

	}