<?php

	namespace Page\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint,
		\L10n\Models\Chani\CmsL10n,
		\Core\Shared;

	class CmsPage extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;
		public $iParentId;
		public $iSequence;
		public $iStatusCode;
		public $sIcon;

		public $_model ='CmsPage';

		public function getSource() {
			return 'page';
		}

		public function initialize() {

			parent::initialize();
			//Get the class
			$sClass = $this->_model;
			$sLocaleClass = $sClass.'Locale';
			$sLocaleClass = $this->testClass(__NAMESPACE__,$sLocaleClass);

			$this->hasMany('id', $sLocaleClass, 'iModelId',array('alias' => 'locales'));
		}

		public function save($aData = null, $aWhiteList = null) {
			$this->dtUpdated = Shared::getDBDate();
			return parent::save();
		}

		/**
		 * @param null $aData
		 * @param null $aWhiteList
		 * Save the locale content for a page
		 */
		public function saveLocales($aData = null, $aWhiteList = null) {
			//Get the editable locales for this record
			$oEditableLocales = $this->getEditableLocales();
			if(count($oEditableLocales) > 0) {
				//Delete the 'live' locales
				$oLocales = $this->getLiveLocales();
				foreach($oLocales AS $oLocale) {
					$oLocale->delete();
				}

				foreach($oEditableLocales AS $oEditableLocale) {
					$oEditableLocale->dtUpdated = Shared::getDBDate();
					$oEditableLocale->bEdit = 0;
					$oEditableLocale->save($aData, $aWhiteList);
				}
			} else {
				echo('Error: no edited locales to save');
				die;
			}
		}

		public function delete() {

			$oLocales = $this->getLocales();
			for($i = 0; $i < count($oLocales); $i++) {
				$oLocales[$i]->delete();
			}

			$oSpice = $this->getSpice();
			for($i = 0; $i < count($oSpice); $i++) {
				$oSpice->delete();
			}

			parent::delete();

		}

		/**
		 * Make the page contents editable
		 */
		public function makeEditable() {
			//Check for existing editable records
			$oEditLocales = $this->getEditableLocales();
			//If none are found, we create them
			if(count($oEditLocales) == 0) {
				$oEditableSpice = $this->getEditableSpice();
				if(count($oEditableSpice) == 0) {
					//Also copy the related spice
					$oSpice = $this->getLiveSpice();
					for($i = 0; $i < count($oSpice); $i++) {
						$oSpice[$i]->makeEditable();
					}
				}
				$oLocales = $this->getLiveLocales();
				for($i = 0; $i < count($oLocales); $i++) {
					$oLocales[$i]->makeEditable();
				}
			}
		}

		/**
		 * @param $id
		 * @param string $title
		 * @param string $position
		 * @return CmsPage
		 * Creates a new page record
		 */
		public static function add($id, $title = '', $position = 'after') {
			$addPage = new CmsPage();

			$addPage->dtCreated = Shared::getDBDate();
			$addPage->dtUpdated = 0;
			$addPage->iStatusCode = 0;
			$addPage->sIcon = '005-newspaper';

			if($position == 'sub') {
				$addPage->iParentId = $id;
				$addPage->iSequence = $addPage->getSequence();
			}

			$addPage = static::movePage($id, $addPage, $position);

			if($addPage->create() == false) {

				$oMessages = $addPage->getMessages();
				foreach($oMessages AS $oMessage) {
					echo $oMessage->getMessage();
				}
				die;

			} else {
				$addPage->createLocales(1,$title);
				return $addPage;
			}
		}

		public static function movePage($iTargetId, $oPage, $position, $bExistingPage = false) {
			$oTarget = CmsPage::findFirst($iTargetId);
			$sequence = $oTarget->iSequence;
			//If we're not adding a sub page, the parent id is the same as the target's
			if($position == 'before' || $position == 'after') {
				$oPage->iParentId = $oTarget->iParentId;
				//Increase if the new page needs to be added after the target
				if($position == 'after') {
					$sequence++;
					$oPage->iSequence = $sequence;
					//If we insert before, the sequence is equal to the target and we increase the target's sequence by 1
				} else {
					$oPage->iSequence = $sequence;
				}
				//Adding a new page is done at the top of the list so sequence is 1
			} elseif($position == 'top') {
				$oPage->iParentId = $id;
				$sequence = 1;
				$oPage->iSequence = $sequence;
			} else {
				$sequence = $oPage->iSequence + 1;
			}
			//Get the remaining pages that need to be resequenced
			$aConditions = [
				'conditions' => 'iParentId = :parentId: AND iSequence >= :sequence:',
				'bind' => [
					'parentId' => $oPage->iParentId,
					'sequence' => $sequence
				],
				'order' => 'iSequence ASC'
			];
			if($bExistingPage) {
				$aConditions['conditions'] .= ' AND id <> :id:';
				$aConditions['bind']['id'] = $oPage->id;
			}
			$pages = CmsPage::find($aConditions);

			if($position == 'before') {
				$oTarget->save();
			}

			//Loop through them and increase the sequence for each,
			foreach($pages AS $page) {
				$sequence++;
				$page->iSequence = $sequence;
				$page->save();
			}
			if($bExistingPage) {
				$oPage->save();
			}
			return $oPage;
		}

		public function getSequence() {
			$SQL = 'SELECT COUNT(id) + 1 AS counter FROM '.get_class($this).' WHERE iParentId = :iParentId:';
			$oQuery = new \Phalcon\Mvc\Model\Query($SQL, $this->getDI());
			$oQuery->setBindParams(array(
				'iParentId' => $this->iParentId
			));
			$oCount = $oQuery->execute();
			return $oCount[0]->counter;
		}

		/**
		 * @param int $bEdit
		 * @param string $title
		 * Creates the locale records for a page
		 */
		public function createLocales($bEdit = 0, $title = '') {
			$oL10n = new CmsL10n();
			$oLanguages = $oL10n->getInputLanguages();
			for($i = 0; $i < count($oLanguages); $i++) {
				CmsPageLocale::add($this->id, $oLanguages[$i]->id, $title, $bEdit);
			}
		}

		/**
		 * @return array
		 * Returns a list of parent ids for the current page
		 */
		public function getParentIds() {

			$aPageIds = array();
			$aPageIds[] = $this->id;

			if($this->iParentId > 0) {
				$oPage = CmsPage::findFirst($this->id);
				while($oPage->iParentId != 0) {
					$oPage = CmsPage::findFirst($oPage->iParentId);
					if($oPage !== false) {
						$aPageIds[] = $oPage->id;
					} else {
						break;
					}
				}
			}
			$aPageIds = array_reverse($aPageIds);
			return $aPageIds;
		}
	}