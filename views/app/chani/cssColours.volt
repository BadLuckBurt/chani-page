input, select, textarea,
button,
.site-header a,
.color-lightest {
    color: {{ colours['lightest'] }};
}

.album-page-list,
body,
.color-lightest-bg {
    background-color: {{ colours['lightest'] }};
}

.site-header a,
.color-lighter {
    color: {{ colours['lighter'] }};
}

.color-light {
    color: {{ colours['light'] }};
}

.album-page-list,
.site-search-nav nav,
.sub-nav,
.color-light-bg {
    background-color: {{ colours['light'] }};
}

.color-dark {
    color: {{ colours['dark'] }};
}

.reveal-button,
.site-search,
.color-dark-bg {
    background-color: {{ colours['dark'] }};
}

.color-darker {
    color: {{ colours['darker'] }};
}

input, select, textarea,
button,
.color-darker-bg {
    background-color: {{ colours['darker'] }};
}

.site-footer,
.site-search-nav nav span,
.site-search-nav a,
.color-darkest {
    color: {{ colours['darkest'] }};
}
.site-header,
.site-search-nav,
.color-darkest-bg {
    background-color: {{ colours['darkest'] }};
}

@media(min-width: 50rem) {
    .site-footer {
        color: {{ colours['light'] }};
    }
}