{% if subElements == true %}
<div id="{{ parentId }}" class="sub-nav">
{% endif %}
<ul>
    {% if subElements == true %}
    <li><span class="menu-back" id="{{ parentId }}-return" onclick="return BitOfAByte.toggleSubNav('{{ parentId }}', this);">&lt; back</span></li>
    {% endif %}
	{% for navEl in navEls %}
	{{ navEl }}
	{% endfor %}
</ul>
{% if subElements == true %}
</div>
{% endif %}