body, 
main {
background-color: {{ colours['lightest'] }};
}

main {
color: {{ colours['darker'] }};
}

main a {
color: {{ colours['darkest'] }};
}
/*
main a:hover {
background-color: {{ colours['darker'] }};
}

main a:hover,
main a:hover img {
color: {{ colours['light'] }};
}
*/