<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width">
    <title>
        
            <?php echo (empty($title) ? ('No title') : ($title)); ?>
        
    </title>
	<link href="<?php echo $this->url->get('public/core/media/favicon.ico?2016'); ?>" rel="shortcut icon" type="image/x-icon" />
    <!--Enter the head scripts and stylesheet here-->
    
        <?php echo (empty($head) ? ('') : ($head)); ?>
    
</head>
<body>
    <!-- Enter the body code / output here -->
    
        <?php echo (empty($body) ? ('') : ($body)); ?>
    
</body>
</html>