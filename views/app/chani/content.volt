{% if bShowTitle == 1 %}
    {% if isSub == true %}
		<div class="pageTitle"data-open="fa-folder-open" data-close="fa-folder">
			<i class="fa fa-folder"></i><h2>{{ title }}</h2>
		</div>
    {% else %}
		<div class="pageTitle">
			<h1>{{ title }}</h1>
		</div>
    {% endif %}
{% endif %}
<div id="blocks" class="blocks">
{% for content in contents %}
    {{content}}
{% endfor %}
</div>