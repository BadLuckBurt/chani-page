<li>
    <a href="<?php echo $url; ?>">
        <?php if ($icon != '') { ?>
        <img src="/public/core/svg/icomoon/SVG/<?php echo $icon; ?>.svg" />
        <?php } ?>
        <?php echo $title; ?>
    </a>
    <?php if ($subNav != '') { ?>
        <span id="<?php echo $titleUrl; ?>-target" onclick="return BitOfAByte.toggleSubNav('<?php echo $titleUrl; ?>', this);">&gt;</span>
        <?php echo $subNav; ?>
    <?php } ?>
</li>