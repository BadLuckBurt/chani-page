<meta name="theme-color" content="#ffffff">
{% for name, values in meta %}
    {% for key, value in values %}
    <meta {{name}}="{{key}}" content="{{value}}" />
    {% endfor %}
{% endfor %}
<!-- CSS inline -->
<style type="text/css">
{% for inline in cssInline %}
{{ inline }}
{% endfor %}
</style>
<!-- CSS -->
{% for sheet in css %}
    <link href="{{ url(sheet) }}" rel="stylesheet" />
{% endfor %}
<!-- Scripts -->
{% for script in scripts %}
	{{ javascript_include(script) }}
{% endfor %}
<script type="text/javascript">
<!--
	window.addEvent('domready',function() {

		{% for code in domready %}
		{{code}}
		{% endfor%}
	});
-->
</script>

