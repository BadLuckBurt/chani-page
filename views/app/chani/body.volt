<header class="site-header">
    <div class="site-header-content">
        <a href="/"><pre id="logo">    ____  _ __           ____            ____        __
   / __ )(_) /_   ____  / __/  ____ _   / __ )__  __/ /____
  / __  / / __/  / __ \/ /_   / __ `/  / __  / / / / __/ _ \
 / /_/ / / /_   / /_/ / __/  / /_/ /  / /_/ / /_/ / /_/  __/
/_____/_/\__/   \____/_/     \__,_/  /_____/\__, /\__/\___/
                                           /____/</pre></a>
    </div>
</header>
<div id="site-search-nav" class="site-search-nav">
    <div class="nav-buttons">
        <button onclick="$('site-search-nav').toggleClass('reveal-search');"><img src="/public/core/svg/icomoon/SVG/135-search.svg" /></button>
        <button type="button" class="menu-button" onclick="$('site-search-nav').toggleClass('reveal-nav');" name="menu-button"><img src="/public/core/svg/icomoon/SVG/190-menu.svg" /></button>
    </div>
    <div class="site-search">
        <button class="toggle-search" type="button" onclick="$('site-search-nav').toggleClass('reveal-search');">&lt;</button>
        <form action="" class="site-search-form">
            <input type="text" name="search" />
            <button type="submit">search</button>
        </form>
    </div>
    <nav id="nav">
        {{ nav|default('') }}
    </nav>
</div>
<main>
    {{ cmsContent }}
    {{content}}
    <div class="clear"></div>
</main>
{{ footer }}