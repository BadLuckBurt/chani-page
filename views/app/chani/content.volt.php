<?php if ($bShowTitle == 1) { ?>
    <?php if ($isSub == true) { ?>
		<div class="pageTitle"data-open="fa-folder-open" data-close="fa-folder">
			<i class="fa fa-folder"></i><h2><?php echo $title; ?></h2>
		</div>
    <?php } else { ?>
		<div class="pageTitle">
			<h1><?php echo $title; ?></h1>
		</div>
    <?php } ?>
<?php } ?>
<div id="blocks" class="blocks">
<?php foreach ($contents as $content) { ?>
    <?php echo $content; ?>
<?php } ?>
</div>