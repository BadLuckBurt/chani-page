<li>
    <a href="{{ url }}">
        {% if icon != '' %}
        <img src="/public/core/svg/icomoon/SVG/{{ icon }}.svg" />
        {% endif %}
        {{ title }}
    </a>
    {% if subNav != '' %}
        <span id="{{ titleUrl }}-target" onclick="return BitOfAByte.toggleSubNav('{{ titleUrl }}', this);">&gt;</span>
        {{ subNav }}
    {% endif %}
</li>