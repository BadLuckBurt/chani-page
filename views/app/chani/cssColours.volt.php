input, select, textarea,
button,
.site-header a,
.color-lightest {
    color: <?php echo $colours['lightest']; ?>;
}

.album-page-list,
body,
.color-lightest-bg {
    background-color: <?php echo $colours['lightest']; ?>;
}

.site-header a,
.color-lighter {
    color: <?php echo $colours['lighter']; ?>;
}

.color-light {
    color: <?php echo $colours['light']; ?>;
}

.album-page-list,
.site-search-nav nav,
.sub-nav,
.color-light-bg {
    background-color: <?php echo $colours['light']; ?>;
}

.color-dark {
    color: <?php echo $colours['dark']; ?>;
}

.reveal-button,
.site-search,
.color-dark-bg {
    background-color: <?php echo $colours['dark']; ?>;
}

.color-darker {
    color: <?php echo $colours['darker']; ?>;
}

input, select, textarea,
button,
.color-darker-bg {
    background-color: <?php echo $colours['darker']; ?>;
}

.site-footer,
.site-search-nav nav span,
.site-search-nav a,
.color-darkest {
    color: <?php echo $colours['darkest']; ?>;
}
.site-header,
.site-search-nav,
.color-darkest-bg {
    background-color: <?php echo $colours['darkest']; ?>;
}

@media(min-width: 50rem) {
    .site-footer {
        color: <?php echo $colours['light']; ?>;
    }
}