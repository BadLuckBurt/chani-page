<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width">
    <title>
        {% block titleTag %}
            {{title|default('No title')}}
        {% endblock %}
    </title>
	<link href="{{ url('public/core/media/favicon.ico?2016')}}" rel="shortcut icon" type="image/x-icon" />
    <!--Enter the head scripts and stylesheet here-->
    {% block headTag %}
        {{head|default('')}}
    {% endblock %}
</head>
<body>
    <!-- Enter the body code / output here -->
    {% block bodyTag %}
        {{body|default('')}}
    {% endblock %}
</body>
</html>