<div class="page-icons">
{% for pageIcon in pageIcons %}
    {%
        if icon == pageIcon
            set iconClass = ' selected'
        else
            set iconClass = ''
        endif
    %}
    <span class="page-icon{{ iconClass }}">
        <img src="/public/core/svg/icomoon/SVG/{{ pageIcon }}.svg" />
    </span>
{% endfor %}
</div>