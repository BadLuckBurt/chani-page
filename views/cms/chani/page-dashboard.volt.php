<div data-target="page-blocks" id="dashboard-content-toolbar" class="dashboard-content-toolbar">
    <div id="dashboard-content-settings-wrapper" class="dashboard-content-settings-wrapper">
        <div class="dashboard-content-settings">
            <h1>Edit page</h1>
            <div class="uniForm">
                <div class="twoCol">
                    <label for="sTitle_<?php echo $id; ?>">Title</label>
                    <input id="sTitle_<?php echo $id; ?>" name="sTitle" value="<?php echo $oLocale->sTitle; ?>" class="page" data-events="blur" data-blur="saveValue|updateTitle" data-locale="true" data-id="<?php echo $id; ?>" data-column="sTitle" type="text">
                </div>
                <div class="twoCol">
                    <label for="iStatusCode_<?php echo $id; ?>">HTTP status code</label>
                    <input id="iStatusCode_<?php echo $id; ?>" name="iStatusCode" value="<?php echo $oPage->iStatusCode; ?>" class="page" data-events="blur" data-blur="saveValue" data-locale="false" data-id="<?php echo $id; ?>" data-column="iStatusCode" type="text">
                </div>
                <div class="twoCol">
                    <label for="iStatus_<?php echo $id; ?>">Active</label>
                    <div class="boxes">
                        <input id="iStatus_<?php echo $id; ?>" <?php echo $iStatusChecked; ?> name="iStatus" value="1" class="page" data-events="click" data-click="toggleValue" data-locale="true" data-id="<?php echo $id; ?>" data-column="iStatus" type="checkbox">
                    </div>
                </div>
                <div class="twoCol">
                    <label for="bShowTitle_<?php echo $id; ?>">Show title</label>
                    <div class="boxes">
                        <input id="bShowTitle_<?php echo $id; ?>" <?php echo $bShowTitle; ?> name="bShowTitle" value="1" class="page" data-events="click" data-click="toggleValue|toggleTitle" data-locale="true" data-id="<?php echo $id; ?>" data-column="bShowTitle" type="checkbox">
                    </div>
                </div>
                <div class="twoCol">
                    <label for="bInMenu_<?php echo $id; ?>">Appears in menu</label>
                    <div class="boxes">
                        <input id="bInMenu_<?php echo $id; ?>" name="bInMenu" <?php echo $bMenuChecked; ?> value="1" class="page" data-events="click" data-click="toggleValue" data-locale="true" data-id="<?php echo $id; ?>" data-column="bInMenu" type="checkbox">
                    </div>
                </div>
                <div class="twoCol">
                    <form id="page-form-reset" action="/chani/page/reset/<?php echo $id; ?>" data-confirm="Are you sure you want to reset your changes for <?php echo $oLocale->sTitle; ?>?">
                        <button type="submit" class="page-cancel cancel floatRight">Reset</button>
                    </form>
                    <form action="/chani/page/save/<?php echo $id; ?>" method="post">
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <button type="submit" class="page-save save floatRight">Save</button>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="buttons-left">
        <a href="" title="Drag to add text" data-target="addspice" data-type="text"><i class="fa fa-2x fa-pencil"></i></a>
        <a href="" title="Drag to add image" data-target="addspice" data-type="image"><i class="fa fa-2x fa-image"></i></a>
        <a href="" title="Drag to add video" data-target="addspice" data-type="video"><i class="fa fa-2x fa-video-camera"></i></a>
        <a href="" title="Drag to add form" data-target="addspice" data-type="form"><i class="fa fa-2x fa-check"></i></a>
    </div>
    <div class="buttons-right">
        <a href="" id="pageSettings" title="Page settings"><i class="fa fa-2x fa-gear"></i></a>
    </div>
    <div class="clear"></div>
</div>
<main>
    <h1 id="page-title"><?php echo $oLocale->sTitle; ?></h1>
    <div id="page-blocks" class="blocks">
        <?php foreach ($spices as $spice) { ?>
            <?php echo $spice; ?>
        <?php } ?>
    </div>
    <div class="clear"></div>
</main>