<h1>Move page</h1>
<div class="uniForm">
    <div class="oneCol">
        <p>Move <span id="movePageTitle"></span> to <span id="moveTargetPageTitle"></span></p>
    </div>
    <div class="oneCol">
        <label>Insert page</label>
        <select id="movePagePosition" name="movePagePosition">
            <option value="before">before</option>
            <option value="after" selected="selected">after</option>
            <option value="sub">as sub page</option>
            <option value="top">top</option>
            <option value="top">bottom</option>
        </select>
    </div>
</div>
<div class="buttons">
    <button class="page-move save" type="button" name="movePage">Move page</button>
    <button class="page-move cancel" type="button" name="moveCancel" data-target="movepage" onclick="return Chani.toggleModalContent(this,false,true,event);">Cancel</button>
</div>
<div class="clear"></div>