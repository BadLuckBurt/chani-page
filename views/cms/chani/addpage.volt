<h1>Add page</h1>
<div class="uniForm">
    <div class="oneCol">
        <label for="addPageTitle">Page title</label>
        <input type="text" name="addPageTitle" id="addPageTitle" />
    </div>
    <div class="oneCol">
        <label>Insert page</label>
        <div class="boxes">
            <input type="radio" name="addPagePosition" id="addPagePositionBefore" value="before" />
            <label for="addPagePositionBefore">before</label>
            <input type="radio" name="addPagePosition" id="addPagePositionAfter" value="after" />
            <label for="addPagePositionAfter">after</label>
            <input type="radio" name="addPagePosition" id="addPagePositionSub" value="sub" />
            <label for="addPagePositionSub">as sub-page</label>
        </div>
    </div>
</div>
<div class="buttons">
    <button class="page save" type="button" name="addPage">Add page</button>
    <button class="page cancel" type="button" name="addCancel" data-target="addpage" onclick="return Chani.toggleModalContent(this,false,true,event);">Cancel</button>
</div>
<div class="clear"></div>