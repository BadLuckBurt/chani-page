<?php

	namespace Page;

	use \Phalcon\Mvc\Router\Group;

	class Routes extends Group
	{
		public function initialize()
		{

			$sFallback = '\Chani';
			$sNameSpaceLower = strtolower(__NAMESPACE__);

			$sControllerNameSpace = __NAMESPACE__.'\Controllers'.$sFallback;

			//Default paths
			$this->setPaths(array(
				'module' => $sNameSpaceLower
			));

			$this->add('/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'App',
				'action'        => 'index',
				'params'        => 1
			));

			$this->add('/chani-ajax/page',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'App',
				'action'        => 'ajax'
			));

			$this->add('/chani/page',array(
				'namespace' => $sControllerNameSpace,
				'controller' => 'Cms',
				'action' => 'index'
			));

			$this->add('/chani/page/:action',array(
				'namespace' => $sControllerNameSpace,
				'controller' => 'Cms',
				'action' => 1
			));

			$this->add('/chani/page/:int',array(
				'namespace' => $sControllerNameSpace,
				'controller' => 'Cms',
				'action' => 'index',
				'params' => 1
			));

			$this->add('/chani/page/:action/:int',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'Cms',
				'action'        => 1,
				'params'        => 2
			));

		}

	}

	return new Routes();