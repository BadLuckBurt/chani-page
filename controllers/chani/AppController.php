<?php

	namespace Page\Controllers\Chani;

	use \Core\Controllers\Chani\AppController AS Controller,
		\Page\Models\Chani\AppPage,
		\Page\Models\Chani\AppPageLocale,
		\Spice\Controllers\Chani\AppController AS SpiceController;

	class AppController extends Controller {

		public $meta = array(
			'name' => array(
				'charset'       => 'UTF-8',
				'keywords'      => 'your, tags',
				'description'   => '150 words',
				'language'      => 'NL',
				'robots'        => 'index,follow'
			)
		);

		public $scripts = array(
			'public/core/js/mootools-core.js',
			'public/modules/page/app/js/chani/Page.js'
		);

		public $cssInline = array();

		public $css = array(
			'public/modules/page/app/css/page.css',
			'public/modules/page/app/css/main.css',
			'public/modules/page/app/css/blocks.css'
		);

		public $domready = array(
			'BitOfAByte.initialize();'
		);
		//TODO: MOVE TO CONFIG
		public $colours = [
			'lightest' => 'rgba(226, 226, 226, 1)',
	        'lighter' => 'rgba(196, 196, 196, 1)',
	        'light' => 'rgba(176, 176, 176, 1)',
	        'dark' => 'rgba(80, 80, 80, 1)',
	        'darker' => 'rgba(40, 40, 40, 1)',
	        'darkest' => 'rgba(20, 20, 20, 1)'
        ];

		public function beforeExecuteRoute($dispatcher) {

		}

		//Renders the resolved page to the user's browser
		//TODO: de-hardcode iLocaleId, needs to be come switchable
		public function indexAction() {
			$iLocaleId = 116;
			$aPageIds = $this->resolveUrlTitles();

			$this->loadPage($aPageIds, $iLocaleId);
		}

		//Handles page HTML loading
		//TODO: Look into HTTP error status handling in Phalcon
		public function loadPage($aPageIds, $iLocaleId) {
			$b404 = false;
			$iPageId = end($aPageIds);
			$oPage = AppPage::findFirst($iPageId);

			//If no page record was found, load the 404
			if($oPage == false) {
				$oPage = AppPage::findFirst(array(
					'conditions' => 'iStatusCode = 404'
				));
				$b404 = true;
			}
			//Load the page content
			if($oPage != false) {
				//Get the localized content
				$oLocale = $oPage->getLocales(array(
					'conditions' => 'bEdit = 0 AND iLocaleId = :iLocaleId:',
					'bind' => array(
						'iLocaleId' => $iLocaleId
					)
				));
				//TODO: Fallback to a default if locale not found
				if($oLocale == false) {
					$oLocale = $oPage->getLocales(array(
						'conditions' => 'bEdit = 0 AND iLocaleId = :iLocaleId:',
						'bind' => array(
							'iLocaleId' => 116
						)
					));
				}
				//Render the HTML for the page
				$oLocale = $oLocale[0];
				$frame = $this->renderPage($oPage, $oLocale, $aPageIds, $iLocaleId);
				//Set header for 404 status
				if($b404 === true) {
					$this->response->setRawHeader('HTTP/1.1 404 Not found');
				}
				//Output HTML to browser
				echo($frame);
			}
		}

		/**
		 * @return string
		 * Renders and returns the HTML for the HEAD tag
		 */
		public function renderHead() {
			$subview = $this->getSubView('page');
			$this->cssInline[] = $subview->render('app/chani/cssColours', [
				'colours' => $this->colours
			]);
			$this->cssInline[] = $subview->render('app/chani/cssMain', [
				'colours' => $this->colours
			]);
			$this->css[] = 'http://fonts.googleapis.com/css?family=Josefine+Sans';
			//Render the HEAD (meta tags, inline / external stylesheets and scripts)
			$head = $subview->render('app/chani/head',array(
				'meta' => $this->meta,
				'cssInline' => $this->cssInline,
				'css' => $this->css,
				'scripts' => $this->scripts,
				'domready' => $this->domready
			));
			return $head;
		}

		/**
		 * @param $oPage
		 * @param $oLocale
		 * @param $aPageIds
		 * @param $iLocaleId
		 * @return mixed|string
		 * Renders the full HTML for a page
		 */
		public function renderPage($oPage, $oLocale, $aPageIds, $iLocaleId) {
			$bEdit = 0;
			//Check for the cached version of this page, return it if found
			$cache = 'page-'.$oLocale->sTitleUrl.'.html';
			$this->view->cache(array(
				'key' => $cache
			));
			if($this->view->getCache()->exists($cache) === true) {
				return $this->view->getCache()->get($cache);
			}
			//Render the content for this page
			$content = $this->getContent($oPage, $oLocale, false, $bEdit, $iLocaleId);
			//TODO: WTF IS ageCounter? Probably remove
			$content = str_replace('|ageCounter|','<span id="ageCounter"></span>',$content);

			//TODO: Necessary or are we going with regular subpages?
			if($oPage->iParentId > 0) {
				$oParent = AppPage::findFirst(array(
					'conditions' => 'id = :id:',
					'bind' => array(
						'id' => $oPage->iParentId
					)
				));
				//Grab the sub pages
				if($oParent->iParentId == 0) {
					$oSubPages = AppPage::find(array(
						'conditions' => 'iParentId = :id:',
						'bind' => array(
							'id' => $oPage->id
						),
						'order' => 'iSequence ASC'
					));
					$aSubContent = array();
					foreach($oSubPages AS $oSubPage) {
						$oSubLocale = $oSubPage->getLocales(array(
							'conditions' => 'bEdit = 0 AND iLocaleId = :iLocaleId: ',
							'bind' => array(
								'iLocaleId' => $iLocaleId
							)
						));
						$oSubLocale = $oSubLocale[0];
						$sSubContent = $this->getContent($oSubPage, $oSubLocale, true, 0, $iLocaleId, false);
						$subview = $this->getSubView('page');
						$aSubContent[] = $subview->render('app/chani/subPage',array(
							'content' => $sSubContent
						));
					}
					$subview = $this->getSubView('page');
					$sSubContent = $subview->render('app/chani/subPages',array(
						'subPages' => $aSubContent
					));
					$content .= "\n".$sSubContent;
				}
			}
			//Render HEAD HTML
			$head = $this->renderHead();
			//Render the navigation HTML
			$aNav = $this->getNavigation(0, $aPageIds, 1);
			$nav = $this->renderNavigation($aNav, true);

			//Render the footer
			$subview = $this->getSubView('page');
			$footer = $subview->render('app/chani/footer', array());

			//Render the BODY (navigation, content and footer)
			$body = $subview->render('app/chani/body',array(
				'nav' => $nav,
				'cmsContent' => '',
				'content' => $content,
				'footer' => $footer
			));

			//TODO: Restore proper caching and TIDY functionality
			//$frame = $this->view->render('app/chani/frame',array(
			$frame = $subview->render('app/chani/frame',array(
				'title' => $oLocale->sTitle,
				'head' => $head,
				'body' => $body
			));
			return $frame;
			//$frame = Shared::tidy($frame, true);
			return $this->view->getCache()->get($cache);
		}

		/**
		 * @param $oPage
		 * @param $oLocale
		 * @param $bSubPage
		 * @param $bEdit
		 * @param $iLocaleId
		 * @return string
		 * Retrieves content blocks and renders them, returns the resulting HTML
		 */
		public function getContent($oPage, $oLocale, $bSubPage, $bEdit, $iLocaleId) {
			$aSpiceContents = $this->getSpice($oPage, $bEdit, $iLocaleId);
			//Render the content
			$subview = $this->getSubView('page');
			return $subview->render('app/chani/content',array(
				'bShowTitle' => $oLocale->bShowTitle,
				'title' => $oLocale->sTitle,
				'contents' => $aSpiceContents,
				'isSub' => $bSubPage
			));
		}


		//TODO: Spice Controller logic should be moved to a helper ASAP
		/**
		 * @param $oPage
		 * @param $bEdit
		 * @param $iLocaleId
		 * @return array
		 * Gets the content blocks for a page
		 */
		public function getSpice($oPage, $bEdit, $iLocaleId) {
			$oSpice = $oPage->getSpice(array(
				'conditions' => 'bEdit = :bEdit: AND sModel = :sModel:',
				'bind' => array(
					'bEdit' => $bEdit,
					'sModel' => $oPage->getSource()
				)
			));
			$oSpiceController = new SpiceController();
			$aSpiceContents = $oSpiceController->process($oSpice, $iLocaleId);
			return $aSpiceContents;
		}

		/**
		 * @param $aPages
		 * @param bool $bSub
		 * @param bool $bSubElements
		 * @param string $sParentId
		 * @param bool $cms
		 * @return string
		 * Renders and returns navigation HTML
		 */
		public function renderNavigation($aPages, $bSub = false, $bSubElements = false, $sParentId = '', $cms = false) {
			$aNavEls = array();
			$subview = $this->getSubView('page');
			for($i = 0; $i < count($aPages); $i++) {

				$sSubNav = '';
				if((count($aPages[$i]['aSub']) > 0) && $bSub == true) {
					$sSubNav = $this->renderNavigation($aPages[$i]['aSub'], $bSub, true, $aPages[$i]['sTitleUrl'],$cms);
				}
				if($cms === true) {
					$url = $this->url->get('chani/page/'.$aPages[$i]['id']);
				} else {
					$url = $this->url->get($aPages[$i]['sUrl']);
				}

				$aNavEls[] = $subview->render('app/chani/navEl',array(
					'class' => $aPages[$i]['sClass'],
					'title' => $aPages[$i]['sTitle'],
					'titleUrl' => $aPages[$i]['sTitleUrl'],
					'icon' => $aPages[$i]['sIcon'],
					'url' => $url,
					'subNav' => $sSubNav,
					'subElements' => $bSubElements,
					'parentId' => $sParentId
				));
			}


			$sNav = $subview->render('app/chani/nav', array(
				'navEls' => $aNavEls,
				'subElements' => $bSubElements,
				'parentId' => $sParentId
			));

			return $sNav;
		}

		/**
		 * @param int $iParentId
		 * @param array $aActivePages
		 * @param int $iMaxLevel
		 * @param int $iLevel
		 * @param int $bEdit
		 * @param int $iLocaleId
		 * @param array $aPages
		 * @return array
		 * Retrieves navigation data
		 */
		public function getNavigation($iParentId = 0, $aActivePages = array(), $iMaxLevel = 0, $iLevel = 0, $bEdit = 0, $iLocaleId = 116, $aPages = array()) {

			$oPages = AppPage::find(array(
				'conditions' => 'iParentId = :iParentId:',
				'bind' => array(
					'iParentId' => $iParentId
				),
				'order' => 'iSequence ASC'
			));

			for($i = 0; $i < count($oPages); $i++) {

				$oLocale = AppPageLocale::findFirst(array(
					'conditions' => 'iLocaleId = :iLocaleId: AND iStatus = 1 AND bEdit = :bEdit: AND iModelId = :iModelId: AND bInMenu = :bInMenu:',
					'bind' => array(
						'iLocaleId' => $iLocaleId,
						'bEdit' => $bEdit,
						'iModelId' => $oPages[$i]->id,
						'bInMenu' => 1
					)
				));

				if($oLocale !== false) {
					if($iParentId == 0) {
						$sClass = 'mainNav';
					} else {
						$sClass = '';
					}
					if(in_array($oPages[$i]->id, $aActivePages)) {
						$sClass = $sClass.' active';
					}
					$aSub = array();
					if($iMaxLevel == 0 || $iLevel < $iMaxLevel) {
						$aSub = $this->getNavigation($oPages[$i]->id, $aActivePages, $iMaxLevel, $iLevel + 1, $bEdit, $iLocaleId);
					}
					$aPages[] = array(
						'id' => $oPages[$i]->id,
						'sClass' => $sClass,
						'sUrl' => $oPages[$i]->createUrl($iLocaleId, $bEdit),
						'sTitle' => $oLocale->sTitle,
						'sTitleUrl' => $oLocale->sTitleUrl,
						'sIcon' => $oPages[$i]->sIcon,
						'aSub' => $aSub
					);
				}

			}
			return $aPages;

		}

		/**
		 * @param null $aParams
		 * @return array
		 * Resolves the URL titles in the matched route and returns a list of page ids
		 */
		public function resolveUrlTitles($aParams = null) {

			$aPageIds = array();
			if($aParams === null) {
				$aParams = $this->dispatcher->getParams();
			}

			if(count($aParams) == 0) {

				//Get the first active page in the top level navigation
				$aPageIds[] = AppPage::getFirstActiveId(0);

			} else {

				$iPageId = 0;

				for($i = 0; $i < count($aParams); $i++) {

					$iPageId = AppPageLocale::resolveUrlTitle($aParams[$i], $iPageId);

					if($iPageId == 0) {

						$aPageIds[] = 0;
						break;

					} else {

						$aPageIds[] = $iPageId;

					}

				}
			}

			return $aPageIds;

		}

		public function _getTranslation($sNameSpace = '') {

			$sNameSpace = __NAMESPACE__;

			return parent::_getTranslation($sNameSpace);

		}

	}
