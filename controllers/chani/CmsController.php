<?php

	namespace Page\Controllers\Chani;

	use \Core\Controllers\Chani\CmsController AS Controller,
		\Page\Models\Chani\CmsPage,
		\Phalcon\DI\FactoryDefault,
		\Page\Models\Chani\CmsPageLocale,
		\Phalcon\Mvc\View\Simple,
		\Spice\Controllers\Chani\CmsController AS SpiceController,
		\Phalcon\Cache\Frontend\Output as OutputFrontend,
		\Phalcon\Cache\Backend\File as FileBackend;

	class CmsController extends Controller {
		public $viewCache;
		//TODO: Move these to a database table
		public $aPageIcons = [
			'005-newspaper',
			'007-pencil2',
			'008-quill',
			'010-blog',
			'014-image',
			'015-images',
			'016-camera',
			'018-music',
			'020-film',
			'022-dice',
			'023-pacman',
			'030-feed',
			'032-book',
			'033-books',
			'036-profile',
			'052-folder-download',
			'070-envelop',
			'076-map'
		];

		//Fields and buttons for model editing
		public $aFormFields = array(
			array(
				'type' => 'oneCol',
				'fields' => array(
					'sTitle' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'true',
						'class' => 'page'
					)
				)
			),
			array(
				'type' => 'twoCol',
				'fields' => array(
					'iStatus' => array(
						'tag' => 'checkField',
						'value' => '1',
						'data-events' => 'click',
						'data-click' => 'toggleValue',
						'data-locale' => 'true',
						'class' => 'page'
					),
					'bShowTitle' => array(
						'tag' => 'checkField',
						'value' => '1',
						'data-events' => 'click',
						'data-click' => 'toggleValue',
						'data-locale' => 'true',
						'class' => 'page'
					)
				)
			),
			array(
				'type' => 'twoCol',
				'fields' => array(
					'bInMenu' => array(
						'tag' => 'checkField',
						'value' => '1',
						'data-events' => 'click',
						'data-click' => 'toggleValue',
						'data-locale' => 'true',
						'class' => 'page'
					),
					'iStatusCode' => array(
						'tag' => 'textField',
						'value' => '',
						'data-events' => 'blur',
						'data-blur' => 'saveValue',
						'data-locale' => 'false',
						'class' => 'page'
					)
				)
			)
		);
		public $aButtons = array(
			'bAdd' => true,
			'bAddSub' => true,
			'bEdit' => true,
			'bDelete' => true,
			'bMove' => true
		);

		//TODO: MOVE TO CONFIG
		public $colours = [
			'lightest' => 'rgba(226, 226, 226, 1)',
			'lighter' => 'rgba(196, 196, 196, 1)',
			'light' => 'rgba(176, 176, 176, 1)',
			'dark' => 'rgba(80, 80, 80, 1)',
			'darker' => 'rgba(40, 40, 40, 1)',
			'darkest' => 'rgba(20, 20, 20, 1)'
		];

		public function initialize() {
			$this->aModules['page']['class'] = 'active';
			$this->aScripts[] = 'public/modules/page/cms/js/chani/Page.js';
			$this->aScripts[] = 'public/core/js/cms/chani/ImageCropper.js';
			$this->aCss[] = 'public/modules/page/app/css/main.css';
			$this->aCss[] = 'public/modules/page/cms/css/cms.css';
		}

		public function onConstruct() {
			$aNameSpace = explode('\\',__NAMESPACE__);
			$this->view = new Simple();
			$this->view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));
			$this->view->setViewsDir(MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/views/');
			$oDi = new FactoryDefault();
			$oDi->set('url', function() {
				$url = new \Phalcon\Mvc\Url();
				$url->setBaseUri('/');
				return $url;
			}, true);
			$this->view->setDI($oDi);

			// Cache data for one day by default
			$frontCache = new OutputFrontend(
				array(
					'lifetime' => 3600
				)
			);

			// Memcached connection settings
			$cache = new FileBackend(
				$frontCache,
				array(
					'prefix' => 'cache-',
					'cacheDir' => BASE_DIR.'/cache/'
				)
			);
			$this->viewCache = $cache;

		}

		public function indexAction() {
			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			//TODO: de-hardcode iLocaleId
			$iLocaleId = 116;
			//TODO: Allow null as page id, show sitemap if encountered, for now force homepage as default
			$id = $this->dispatcher->getParam(0);
			if($id === null) {
				$id = 0;
			}
			//Prepare additional dom loading events to assign events to formfields / buttons
			$this->aDomready[] = "
			var CmsPage = new chaniPage({
				id: $id,
				iLocaleId: $iLocaleId, 
				ajax: {
					addSpiceUrl:    '/chani/spice/add',
					saveValueUrl:   '/chani/page/saveValue/$id',
					addPageUrl:     '/chani/page/add',
					deletePageUrl:  '/chani/page/delete',
					movePageUrl:    '/chani/page/move',
					previewUrl:     '/chani/page/preview',
					cancelUrl:      '/chani/page/cancel',
					resetUrl:       '/chani/page/reset'
				},
				cancelMessage: '',
				className: 'page',
				master: true
			});";
			//Prepare checked attributes for checkbox / radiofield formfields
			$pageSettings = [
				'id' => $id,
				'iStatusChecked' => '',
				'bMenuChecked' => '',
				'bShowTitle' => '',
				'oPage' => null,
				'oLocale' => null
			];
			//Find the page to edit and make the locales and blocks editable
			if($id > 0) {
				$CmsPage = CmsPage::findFirst($id);
				$CmsPage->makeEditable();
				$aParentIds = $CmsPage->getParentIds();
				//Retrieve the editable locale
				$oLocale = $CmsPage->getLocales(array(
					'conditions' => 'bEdit = 1 AND iLocaleId = :iLocaleId:',
					'bind' => array(
						'iLocaleId' => $iLocaleId
					)
				));
				if($oLocale[0]->iStatus == 1) {
					$pageSettings['iStatusChecked'] = 'checked="checked"';
				}
				if($oLocale[0]->bShowTitle == 1) {
					$pageSettings['bShowTitle'] = 'checked="checked"';
				}
				if($oLocale[0]->bInMenu == 1) {
					$pageSettings['bMenuChecked'] = 'checked="checked"';
				}
				$pageSettings['oPage'] = $CmsPage;
				$pageSettings['oLocale'] = $oLocale[0];

				$cSpice = new SpiceController();
				//Retrieve the editable blocks for the page
				$pageSpice = $CmsPage->getEditableSpice();
				$pageSettings['spices'] = $cSpice->process($pageSpice, $iLocaleId);
				//Render the page settings HTML
				$cmsContent = $this->view->render('cms/chani/page-dashboard', $pageSettings);

				//Add required spice scripts and dom loading events
				$this->aScripts = array_merge($this->aScripts, $cSpice->aScripts);
				$this->aDomready[] = $cSpice->getEditDomready('page', $CmsPage->id);

			} else {
				$aParentIds = [];
				$cmsContent = '';
			}
			$sTitle = $oTranslator->_('overviewTitle');
			$sitemapRender = $this->renderSitemap($aParentIds);

			$this->aCssInline[] = $this->view->render('app/chani/cssMain', [
				'colours' => $this->colours
			]);
			//Render the HEAD HTML
			$sHead = $this->renderHead();

			//Prepare the HTML for dashboard modals
			$aModelInfo = [
				'model' => 'page',
				'modelId' => $id
			];
			$dashboardModals = $this->getDashboardModals($aModelInfo);

			$dbContent = [
				'sitemap' => $sitemapRender,
				'library' => '<h1>Media library placeholder</h1>'
			];
			$body = $this->renderBody($cmsContent, $dbContent, $dashboardModals, false);

			//Render and display full HTML
			$sHtml = $this->renderHTML($sTitle, $sHead, $body, true);
			echo($sHtml);
		}

		//Create a new page record
		public function addAction() {

			//Retrieve post or throw error
			//TODO: Implement error handling
			if($this->request->isPost()) {
				$iParentId = $this->request->getPost('id',null,0);
				$title = $this->request->getPost('title',null,'');
				$position = $this->request->getPost('position',null,0);

				//Create the new page record
				$oPage = CmsPage::add($iParentId, $title, $position);
				$aParentIds = $oPage->getParentIds();
				$aPages = $this->buildOverview();
				$sOverview = '';
				$oTranslator = $this->_getTranslation(__NAMESPACE__);
				$this->swapViewDir();
				for($i = 0; $i < count($aPages); $i++) {
					$sOverview .= $this->renderOverviewRow($aPages[$i], $this->aButtons, $aPages[$i]['iSequence'], $oTranslator, 'page', $aParentIds);
				}
				$this->restoreViewDir();
				echo($sOverview);

			} else {
				echo('Error: no page data posted');
				die;
			}
		}

		//Move a page to a different hierarchy position, updating sequence in the process
		public function moveAction() {
			if($this->request->isPost()) {

				$aPost = $this->request->getPost();

				$id = $aPost['id']; //The id of the page to resequence
				$iTargetId = $aPost['targetId'];   //The id of the target
				$position = $aPost['position'];

				$oTarget = CmsPage::findFirst($iTargetId);
				if($position == 'sub') {
					$iParentId = $iTargetId;
				} else {
					$iParentId = $oTarget->iParentId;
				}
				$oPage = CmsPage::findFirst($id);

				$iOldParent = -1;
				if($oPage->iParentId != $iParentId) {
					$iOldParent = $oPage->iParentId;
				}
				$oPage->iParentId = $iParentId;
				$oPage = CmsPage::movePage($iTargetId, $oPage, $position, true);

				if($iOldParent > -1) {
					$oPages = CmsPage::find(array(
						'conditions' => 'iParentId = :iParentId:',
						'bind' => array(
							'iParentId' => $iOldParent
						),
						'order' => 'iSequence ASC'
					));
					$iSequence = 0;
					for($i = 0; $i < count($oPages); $i++) {
						$iSequence = $iSequence + 1;
						$oPages[$i]->iSequence = $iSequence;
						$oPages[$i]->save();
					}
				}
				$aParentIds = $oPage->getParentIds();
				$aPages = $this->buildOverview();
				$sOverview = '';
				$oTranslator = $this->_getTranslation(__NAMESPACE__);
				$this->swapViewDir();
				for($i = 0; $i < count($aPages); $i++) {
					$sOverview .= $this->renderOverviewRow($aPages[$i], $this->aButtons, $aPages[$i]['iSequence'], $oTranslator, 'page', $aParentIds);
				}
				$this->restoreViewDir();
				echo($sOverview);
				die;

			} else {
				echo('no page id and target posted');
				die;
			}
		}

		//Removes all editable content versions,
		//triggering the re-creation of editable content from original content
		public function resetAction() {
			$aParams = $this->dispatcher->getParams();
			if(count($aParams) === 0) {
				echo('No page id found');
				die;
			} else {
				$id = $aParams[0];
				$oPage = CmsPage::findFirst($id);
				if($oPage === false) {
					echo('Page '.$id.' does not exist');
					die;
				} else {
					$oEditableSpice = $oPage->getEditableSpice();
					foreach($oEditableSpice AS $oEditable) {
						$oEditable->delete();
					}
					$oEditables = $oPage->getEditableLocales();
					foreach($oEditables AS $oEditable) {
						$oEditable->delete();
					}
					$this->response->redirect('chani/page/'.$id);
				}
			}
		}

		//Save changes to editable content and make them definitive
		public function saveAction() {

			$id = $this->request->getPost('id',null,0);

			if($id > 0) {

				$oPage = CmsPage::findFirst($id);
				$oPage->saveData(true);
				$oPage->saveLocales();
				$oLocales = $oPage->getLocales();
				//$this->view->cache();
				//$oCache = $this->view->getCache();
				if(2 == 3) {
					foreach ($oLocales AS $oLocale) {
						$oCache->delete('page-' . $oLocale->sTitleUrl . '.html');
					}
				}
			}
			$this->response->redirect('chani/page/');
		}

		//Delete a page, related locales and spice contents
		public function deleteAction() {

			if($this->request->isPost()) {
				$post = $this->request->getPost();
			} else {
				echo('no page id posted');
				die;
			}

			$id = $post['id'];
			$oPage = CmsPage::findFirst($id);

			if($oPage !== false) {
				$iParentId = $oPage->iParentId;
				$aParentIds = $oPage->getParentIds();
				$oPage->delete();

				$pages = CmsPage::find([
					'conditions' => 'iParentId = :parentId:',
					'bind' => ['parentId' => $iParentId],
					'order' => 'iSequence ASC'
				]);

				$sequence = 0;
				for($i = 0; $i < count($pages); $i++) {
					$sequence++;
					$pages[$i]->iSequence = $sequence;
					$pages[$i]->save();
				}

				$aPages = $this->buildOverview();
				$sOverview = '';
				$oTranslator = $this->_getTranslation(__NAMESPACE__);
				$this->swapViewDir();
				for($i = 0; $i < count($aPages); $i++) {
					$sOverview .= $this->renderOverviewRow($aPages[$i], $this->aButtons, $aPages[$i]['iSequence'], $oTranslator, 'page', $aParentIds);
				}
				$this->restoreViewDir();
				echo($sOverview);
				die;
			}

		}

		//Save a page value (triggered from the editing form)
		public function saveValueAction() {

			$aPost = $this->request->getPost();
			$id = $aPost['id'];
			$sValue = $aPost['sValue'];
			$sColumn = $aPost['sColumn'];
			$iLocaleId = $aPost['iLocaleId'];
			$bLocale = $aPost['bLocale'];
			$oPage = CmsPage::findFirst($id);

			if($bLocale === 'true') {

				$oEditable = $oPage->getLocales(array(
					'bEdit = 1 AND iLocaleId = :iLocaleId:',
					'bind' => array(
						'iLocaleId' => $iLocaleId
					)
				));

				$oEditable[0]->$sColumn = $sValue;

				$oEditable[0]->save(false);

			} else {

				$oPage->$sColumn = $sValue;
				$oPage->save(false);

			}

			echo('success');

		}

		//Rebuilds and renders an Overview list for the sitemap
		public function refreshOverviewAction() {
			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			$aPages = $this->buildOverview();
			echo $this->renderOverviewBody($aPages, $oTranslator, $this->aButtons);
		}

		/**
		 * @param int $iParentId
		 * @param array $aPages
		 * @return array
		 * Builds the page overview data structure
		 */
		public function buildOverview($iParentId = 0, $aPages = array()) {
			$oPages = CmsPage::find(array(
				'conditions' => 'iParentId = :iParentId:',
				'order' => 'iSequence ASC',
				'bind' => array(
					'iParentId' => $iParentId
				)
			));

			$oTranslator = $this->_getTranslation(__NAMESPACE__);

			for($i = 0; $i < count($oPages); $i++) {

				$aSubPages = $this->buildOverview($oPages[$i]->id);

				$oPageLocale = CmsPageLocale::findFirst(array(
					'conditions' => 'bEdit = 0 AND iModelId = :iModelId: AND iLocaleId = 116',
					'bind' => array(
						'iModelId' => $oPages[$i]->id
					),
					'columns' => array('sTitle','iStatus')
				));

				if($oPageLocale === false) {
					$oPageLocale = CmsPageLocale::findFirst(array(
						'conditions' => 'bEdit = 1 AND iModelId = :iModelId: AND iLocaleId = 116',
						'bind' => array(
							'iModelId' => $oPages[$i]->id
						),
						'columns' => array('sTitle', 'iStatus')
					));
				}

				$sClass = '';

				if($oPageLocale->iStatus == 1) {

					$sClass = 'active';

				}

				$aPages[] = $this->buildOverviewRow($oPages[$i], $oPageLocale, $sClass, $aSubPages, $oTranslator);

			}

			return $aPages;

		}

		/**
		 * @param $oPage
		 * @param null $oLocale
		 * @param $sClass
		 * @param $aSubPages
		 * @param $oTranslator
		 * @return array
		 * Build a page row data structure for the overview
		 */
		public function buildOverviewRow($oPage, $oLocale = null, $sClass, $aSubPages, $oTranslator) {
			if($oLocale !== null) {
				$sTitle = $oLocale->sTitle;
			} else {
				$sTitle = $oTranslator->_('newPage');
			}
			return [
				'id' => $oPage->id,
				'htmlId' => 'page-'.$oPage->id,
				'data-target' => 'movepage',
				'sTitle' => $sTitle,
				'iSequence' => $oPage->iSequence,
				'class' => $sClass,
				'sub' => $aSubPages,
				'addUrl' => 'chani/page/add/'.$oPage->id,
				'editUrl' => 'chani/page/'.$oPage->id,
				'deleteUrl' => 'chani/page/delete/'.$oPage->id,
				'confirmDelete' => $oTranslator->_('confirmDelete').' '.$sTitle
			];
		}

		public function renderSitemap($aParentIds = []) {
			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			//Build the overview list for the sitemap
			$aPages = $this->buildOverview(0);
			//Render sitemap HTML
			$sitemap = $this->renderOverview($aPages, 'page', $oTranslator, $this->aButtons, $aParentIds);
			//Render the sitemap for the Page module
			return $this->view->render('cms/chani/dashboard.content.sitemap',[
				'sitemap' => $sitemap
			]);
		}

		public function getDashboardModals($aModelInfo) {
			$cSpice = new SpiceController();
			$spiceModals = $cSpice->getDashboardModals($aModelInfo);
			$dashboardModals = [
				'addpage' => ['html' => $this->view->render('cms/chani/addpage',[]),'buttons' => ''],
				'movepage' => ['html' => $this->view->render('cms/chani/movepage',[]),'buttons' => ''],
				'library' => ['html' => '<h1>Media modal placeholder</h1>','buttons' => '']
			];
			return array_merge($dashboardModals, $spiceModals);
		}

		public function _getTranslation($sNameSpace = '') {

			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}

			return parent::_getTranslation($sNameSpace);

		}

	}